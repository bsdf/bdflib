// This file is part of bdf_parse.

// bdf_parse is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// bdf_parse is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with bdf_parse.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>

#include "magick.hh"

void
write_glyph_to_file(bdf_glyph glyph, std::string file_name)
{
  std::cout << "writing glyph: " << glyph.name
            << " to: " << file_name
            << std::endl;

  Magick::Image image(Magick::Geometry(16, glyph.bitmap_data.size()), COLOR_WHITE);
  // image.transparent(COLOR_TRANS);

  auto bitmap_data = glyph.bitmap_data;
  for (int i = 0; i < bitmap_data.size(); i++)
    {
      auto bits = bitmap_data[i];
      auto bitsize = bits.size();
      for (int j = 0; j < bitsize; j++)
        {
          if (bits[bitsize-j])
            {
              image.pixelColor(j, i, COLOR_BLACK);
            }
        }
    }

  // image.trim();
  image.transparent(COLOR_WHITE);
  image.write(file_name);
}
