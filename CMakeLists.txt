cmake_minimum_required(VERSION 3.1)
set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

project(bdf_parse)

find_package(ImageMagick COMPONENTS Magick++ MagickCore)

add_executable(bdf_parse
  bdf_parse.cc
  bdflib.cc
  magick.cc
  bdf.c
  bdfgname.c
  bdfgrid.c)

set_target_properties(bdf_parse PROPERTIES
  CXX_STANDARD          17
  CXX_STANDARD_REQUIRED ON
  CXX_EXTENSIONS        OFF)

target_include_directories(bdf_parse PUBLIC ${ImageMagick_INCLUDE_DIRS})
target_link_libraries(bdf_parse ${ImageMagick_LIBRARIES} stdc++fs)
target_compile_definitions(bdf_parse PUBLIC
  MAGICKCORE_HDRI_ENABLE=0
  MAGICKCORE_QUANTUM_DEPTH=16)
