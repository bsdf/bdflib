// This file is part of bdf_parse.

// bdf_parse is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// bdf_parse is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with bdf_parse.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <filesystem>

#include "bdflib.hh"
#include "magick.hh"

namespace fs = std::filesystem;

void
write_images(bdf_font font)
{
  std::cout << "writing images to ./pngs" << std::endl;

  fs::path output_dir("./pngs");
  if (!fs::exists(output_dir))
    {
      fs::create_directory(output_dir);
    }

  for (auto g : font.glyphs)
    {
      std::string file_name = "./pngs/" + g.name + ".png";
      write_glyph_to_file(g, file_name);
    }
}

int
main(int argc, char **argv)
{
  if (argc == 1)
    {
      std::cerr << "font file not specified." << std::endl;
      exit(1);
    }

  std::string font_name = argv[1];
  bdf_font font(font_name);
  write_images(font);
  
  return 0;
}
