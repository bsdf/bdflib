// This file is part of bdf_parse.

// bdf_parse is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// bdf_parse is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with bdf_parse.  If not, see <https://www.gnu.org/licenses/>.

#include <cstdio>

#include <bitset>
#include <string>
#include <vector>

#include "bdf.h"

#ifndef BDFLIB_H
#define BDFLIB_H

#define BITSET_BITS 16

typedef std::bitset<BITSET_BITS> glyph_bitset;
typedef std::vector<glyph_bitset> bitset_vector;

class bdf_glyph
{
public:
  std::string name;
  bitset_vector bitmap_data;

  bdf_glyph(bdf_glyph_t g)
  {
    name = g.name;
    bitmap_data = get_glyph_bitsets(g);
  };

private:
  bitset_vector get_glyph_bitsets(bdf_glyph_t glyph);
};

class bdf_font
{
public:
  std::string name;
  std::vector<bdf_glyph> glyphs;

  bdf_font(std::string font_file);
};

#endif /* BDFLIB_H */
