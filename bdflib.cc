// This file is part of bdf_parse.

// bdf_parse is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// bdf_parse is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with bdf_parse.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>

#include "bdflib.hh"

bitset_vector
bdf_glyph::get_glyph_bitsets(bdf_glyph_t glyph)
{
  bitset_vector bitsets;
  for (int i = 0; i < glyph.bytes; i++)
    {
      bitsets.push_back(glyph_bitset(glyph.bitmap[i]));
    }

  return bitsets;
}

bdf_font::bdf_font(std::string font_file)
{
  bdf_options_t opts;
  bdf_font_t   *font;
  bdf_setup();

  FILE *in = std::fopen(font_file.c_str(), "r");
  if (in == nullptr)
    {
      std::cerr << "file not found." << std::endl;
      exit(1);
    }

  bdf_default_options(&opts);
  font = bdf_load_font(in, &opts, 0, 0);

  for (int i = 0; i < font->glyphs_used; i++)
    {
      auto glyph = font->glyphs[i];
      glyphs.push_back(bdf_glyph(glyph));
    }

  name = font->name;

  fclose(in);
  bdf_cleanup();
}

/*
00000000
00000000
00000000
00000000
00011000
00100100
00100100
01000010
01000010
01111110
01000010
01000010
01000010
01000010
00000000
00000000
*/
